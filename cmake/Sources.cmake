# WARNING if you add a new set of sources that contain .F90 files 
# remember to add it to the collection of all fortran sources
# AT_SOURCE_FILES
set(AT_TYPE_FILES
    src/AT-type.F90
    )
set(AT_CORE_FILES
    src/AT-permute.F90
    src/AT-dim-opr.F90
    src/AT-flop-opr.F90
    )
set(AT_ROUTINE_FILES
    src/AT-contract-opr.F90
    )
set(AT_TESTER
    src/AT-tester.F90
    )
