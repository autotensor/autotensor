set(LSDALTON_EXTERNAL_LIBS)

if(ENABLE_SCALASCA)
    set(SCALASCA_INSTRUMENT ${CMAKE_Fortran_COMPILER})
    configure_script(
        ${CMAKE_SOURCE_DIR}/LSDALTON/scalasca.in
        ${PROJECT_BINARY_DIR}/scalascaf90.sh
        )
    set(SCALASCA_INSTRUMENT ${CMAKE_C_COMPILER})
    configure_script(
        ${CMAKE_SOURCE_DIR}/LSDALTON/scalasca.in
        ${PROJECT_BINARY_DIR}/scalascaCC.sh
        )
    set(SCALASCA_INSTRUMENT ${CMAKE_CXX_COMPILER})
    configure_script(
        ${CMAKE_SOURCE_DIR}/LSDALTON/scalasca.in
        ${PROJECT_BINARY_DIR}/scalascaCXX.sh
        )
    unset(SCALASCA_INSTRUMENT)
    SET(CMAKE_Fortran_COMPILER "${PROJECT_BINARY_DIR}/scalascaf90.sh")
    SET(CMAKE_C_COMPILER "${PROJECT_BINARY_DIR}/scalascaCC.sh")
    SET(CMAKE_CXX_COMPILER "${PROJECT_BINARY_DIR}/scalascaCXX.sh")
endif()
if(ENABLE_VAMPIRTRACE)
    SET(CMAKE_Fortran_COMPILER "vtfort")
    SET(CMAKE_C_COMPILER "vtcc")
    SET(CMAKE_CXX_COMPILER "vtc++")
endif()


add_library(
    at_type
    ${AT_TYPE_FILES}
    )

add_library(
    at_core
    ${AT_CORE_FILES}
    )

target_link_libraries(at_core at_type)

add_library(
    at_routines
    ${AT_ROUTINE_FILES}
    )
target_link_libraries(at_routines at_core )

if(ENABLE_GPU)
   set(reorder_definitions "--acc ${reorder_definitions}")
endif()
if(ENABLE_REAL_SP)
   set(reorder_definitions "--real_sp ${reorder_definitions}")
endif()

#add_dependencies(lsutiltypelib_common matrixulib)
#target_link_libraries(lsutil_tensor_lib matrixmlib )

set(ExternalProjectCMakeArgs
    -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
    -DCMAKE_INSTALL_PREFIX=${PROJECT_BINARY_DIR}/external
    -DCMAKE_Fortran_COMPILER=${CMAKE_Fortran_COMPILER}
    -DENABLE_64BIT_INTEGERS=${ENABLE_64BIT_INTEGERS}
    -DPARENT_MODULE_DIR=${PROJECT_BINARY_DIR}/modules
    )

if(ENABLE_CUDA)
    find_package(CUDA)
endif()
if(CUDA_FOUND)
    # this below is a bit convoluted but here we make
    # sure that the CUDA sources are compiled with GNU always
    # this makes life easier if LSDalton is compiled with Intel
    add_definitions(-DENABLE_CUDA)
    set(ExternalProjectCMakeArgs
        -DCMAKE_C_COMPILER=gcc
        -DCMAKE_CXX_COMPILER=g++
        )
    ExternalProject_Add(cuda_interface
        SOURCE_DIR  ${PROJECT_SOURCE_DIR}/LSDALTON/cuda
        BINARY_DIR  ${PROJECT_BINARY_DIR}/cuda/build
        STAMP_DIR   ${PROJECT_BINARY_DIR}/cuda/stamp
        TMP_DIR     ${PROJECT_BINARY_DIR}/cuda/tmp
        DOWNLOAD_COMMAND ""
        INSTALL_COMMAND ""
        )
    include_directories(${PROJECT_SOURCE_DIR}/src/cuda)
    set(LSDALTON_EXTERNAL_LIBS
        ${PROJECT_BINARY_DIR}/cuda/build/libcuda_interface.a
        ${CUDA_LIBRARIES}
        ${LSDALTON_EXTERNAL_LIBS}
        )
    add_dependencies(lsdaltonmain cuda_interface)
endif()

#DO NOT ALWAYS USE stdc++ SINCE THIS IS ONLY!!!! THE GNU STDC++ LIB
#if(CMAKE_Fortran_COMPILER_ID MATCHES Cray)
#   set(USE_GNU_STDCXX_LIB "")
#else()
#   set(USE_GNU_STDCXX_LIB "stdc++")
#endif()

if(NOT AT_LIBRARY_ONLY)
    if(MPI_FOUND)
        # Simen's magic fix for Mac/GNU/OpenMPI
        if(${CMAKE_SYSTEM_NAME} STREQUAL "Darwin")
            if(CMAKE_Fortran_COMPILER_ID MATCHES GNU)
                SET_TARGET_PROPERTIES(ATtest.x     PROPERTIES LINK_FLAGS "-Wl,-commons,use_dylibs")
            endif()
        endif()
    endif()

    add_executable(
        ATtest.x
        ${CMAKE_SOURCE_DIR}/src/AT-tester.F90
        ${LINK_FLAGS}
        )

    target_link_libraries(
        ATtest.x
        at_type
        at_core
        at_routines
        ${EXTERNAL_LIBS}
        ${USE_GNU_STDCXX_LIB}
        )

endif()


