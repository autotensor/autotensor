macro(add_attensor_test _name _labels)
    add_test(${_name} ${CMAKE_BINARY_DIR}/ATtest.x > ${_name}; paste ${_name} )
    if(NOT "${_labels}" STREQUAL "")
        set_tests_properties(${_name} PROPERTIES LABELS "${_labels}")
    endif()
endmacro()

macro(add_attensor_runtest _name _labels)
    add_test(
        ${_name}
        python ${CMAKE_SOURCE_DIR}/test/${_name}/test --binary-dir=${PROJECT_BINARY_DIR} --work-dir=${PROJECT_BINARY_DIR}/test/${_name} --verbose --log=${PROJECT_BINARY_DIR}/test/${_name}/runtest.stderr.log)
    if(NOT "${_labels}" STREQUAL "")
        set_tests_properties(${_name} PROPERTIES LABELS "${_labels}")
    endif()
endmacro()

add_attensor_runtest(attest "attest")

#add_lsdalton_test(linsca/linsca_trilevel_2nd_all                 "linsca")
#add_lsdalton_runtest(linsca/linsca_atoms                         "linsca")
