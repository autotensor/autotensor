
      module contraction

       use AT_type_def
       use AT_contract_mod
     
       implicit none

       private

       public contraction1b_2_2 
       public contraction1b_2_4 
       public contraction1b_4_2 
       public contraction1b_4_4 
       public contraction1b_4_6 
       public contraction1b_2_2_2 
!      public contraction1b_2_2_4 
!      public contraction1b_2_4_4 
       public contraction1b_4_2_2 
       public contraction1b_4_2_4 
!      public contraction1b_4_4_4 
       public contraction1b_4_2_2_2 
!      public contraction1b_4_2_2_4 
!      public contraction1b_4_2_4_4 
!      public contraction1b_4_4_4_4 

       public contraction2b_2_4 
       public contraction2b_2_2_4 
       public contraction2b_4_2 
       public contraction2b_4_4 
       public contraction2b_2_6 
       public contraction2b_4_6 
       public contraction2b_4_2_2 
       public contraction2b_4_2_4 
       public contraction2b_4_4_4 
       public contraction2b_4_2_2_2 
       public contraction2b_4_2_2_4 
       public contraction2b_4_2_2_2_2 

       public contraction1b_4_4_2 ! Not reqd. same as contraction1b_4_2_4

       public contraction3b_4_4
       public contraction3b_4_2_4
       public contraction3b_2_4_4
       public contraction3b_4_4_4
       public contraction3b_4_2_4_4

       TYPE(AT_tensor) :: A,B,C,D,E,R
       Integer :: i,j,k,l,m,n,p,q,ij,kl

       contains

       subroutine contraction1b_2_2(AA,A_dim,adim,BB,B_dim,bdim,RR,r1,r2,fact,  &          
        &                     n1,n2,n3,n4,n5,n6)

       implicit none

       integer,intent(in) ::adim,bdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6
       Integer,intent(in) :: r1,r2
       real*8, intent(in), target::AA(A_dim(1),A_dim(2))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2)
       
       
!      IF (A_dim(1).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_2_2!'
!      ENDIF
!      IF (A_dim(2).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_2_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(R,(/ r1,r2 /) )
       
       
       !initialize A
        call array_to_AT_tensor_1b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       call at_contract(R,(/ n1,n2 /),A,(/ n3,n4 /),B,(/ n5,n6 /))

       m=0
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j)=RR(i,j) + fact*R%elms(m)
       ENDDO
       ENDDO
       
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(R)
       
       END subroutine contraction1b_2_2
        
       subroutine contraction1b_2_4(AA,A_dim,adim,BB,B_dim,bdim,RR,r1,r2,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8)

       implicit none

       integer,intent(in) ::adim,bdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8
       Integer,intent(in) :: r1,r2
       real*8, intent(in), target::AA(A_dim(1),A_dim(2))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2),B_dim(3),B_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_2_4!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_2_4!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2),B_dim(3),B_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2 /) )
       
       
       !initialize A
        call array_to_AT_tensor_1b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_2b(BB,B_dim,B)
       
       call at_contract(R,(/ n1,n2 /),A,(/ n3,n4 /),B,(/ n5,n6,n7,n8 /))

       m=0
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j)=RR(i,j) + fact*R%elms(m)
       ENDDO
       ENDDO
       
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(R)
       
       END subroutine contraction1b_2_4
        
       subroutine contraction1b_4_2(AA,A_dim,adim,BB,B_dim,bdim,RR,r1,r2,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8)

       implicit none

       integer,intent(in) ::adim,bdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8
       Integer,intent(in) :: r1,r2
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(R,(/ r1,r2 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       call at_contract(R,(/ n1,n2 /),A,(/ n3,n4,n5,n6 /),B,(/ n7,n8 /))

       m=0
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j)=RR(i,j) + fact*R%elms(m)
       ENDDO
       ENDDO
       
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(R)
       
       END subroutine contraction1b_4_2
        
       subroutine contraction1b_4_4(AA,A_dim,adim,BB,B_dim,bdim,RR,r1,r2,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10)

       implicit none

       integer,intent(in) ::adim,bdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10
       Integer,intent(in) :: r1,r2
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2),B_dim(3),B_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_4!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_4!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2),B_dim(3),B_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_2b(BB,B_dim,B)
       
       call at_contract(R,(/ n1,n2 /),A,(/ n3,n4,n5,n6 /),B,(/ n7,n8,n9,n10 /))

       m=0
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j)=RR(i,j) + fact*R%elms(m)
       ENDDO
       ENDDO

       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(R)
       
       END subroutine contraction1b_4_4
        
       subroutine contraction1b_2_2_2(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,RR,r1,r2,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8)

       implicit none

       integer,intent(in) ::adim,bdim,cdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8
       Integer,intent(in) :: r1,r2
       real*8, intent(in), target::AA(A_dim(1),A_dim(2))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_2_2_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_2_2_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2) /) )
        call alloc_at_tensor(R,(/ r1,r2 /) )
       
       
       !initialize A
        call array_to_AT_tensor_1b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_1b(CC,C_dim,C)
       
       call at_contract(R,(/ n1,n2 /),A,(/ n3,n4 /),B,(/ n5,n6 /),C, (/ n7,n8 /))

       m=0
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j)=RR(i,j) + fact*R%elms(m)
       ENDDO
       ENDDO
       
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(R)
       
       END subroutine contraction1b_2_2_2

       subroutine contraction1b_4_2_2(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,RR,r1,r2,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10)

       implicit none

       integer,intent(in) ::adim,bdim,cdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10
       Integer,intent(in) :: r1,r2
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2) /) )
        call alloc_at_tensor(R,(/ r1,r2 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_1b(CC,C_dim,C)
       
       call at_contract(R,(/ n1,n2 /),A,(/ n3,n4,n5,n6 /),B,(/ n7,n8 /),C, (/ n9,n10 /))

       m=0
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j)=RR(i,j) + fact*R%elms(m)
       ENDDO
       ENDDO
       
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(R)
       
       END subroutine contraction1b_4_2_2

       subroutine contraction1b_4_2_4(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,RR,r1,r2,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12)

       implicit none

       integer,intent(in) ::adim,bdim,cdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12
       Integer,intent(in) :: r1,r2
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2),C_dim(3),C_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2),C_dim(3),C_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_2b(CC,C_dim,C)
       
       call at_contract(R,(/ n1,n2 /),A,(/ n3,n4,n5,n6 /),B,(/ n7,n8 /),C, (/ n9,n10,n11,n12 /))

       m=0
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j)=RR(i,j) + fact*R%elms(m)
       ENDDO
       ENDDO
       
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(R)
       
       END subroutine contraction1b_4_2_4

       subroutine contraction1b_4_4_2(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,RR,r1,r2,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12)

       implicit none

       integer,intent(in) ::adim,bdim,cdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12
       Integer,intent(in) :: r1,r2
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2),B_dim(3),B_dim(4))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2),B_dim(3),B_dim(4) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2) /) )
        call alloc_at_tensor(R,(/ r1,r2 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_2b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_1b(CC,C_dim,C)
       
       call at_contract(R,(/ n1,n2 /),A,(/ n3,n4,n5,n6 /),B,(/ n7,n8,n9,n10 /) ,C,(/ n11,n12 /))

       m=0
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j)=RR(i,j) + fact*R%elms(m)
       ENDDO
       ENDDO
       
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(R)
       
       END subroutine contraction1b_4_4_2

       subroutine contraction1b_4_2_2_2(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,DD,D_dim,ddim1,RR,r1,r2,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12)

       implicit none

       integer,intent(in) ::adim,bdim,cdim,ddim1
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim),D_dim(ddim1)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12
       Integer,intent(in) :: r1,r2
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2))
       real*8, intent(in), target::DD(D_dim(1),D_dim(2))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(D)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2) /) )
        call alloc_at_tensor(D,(/ D_dim(1),D_dim(2) /) )
        call alloc_at_tensor(R,(/ r1,r2 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_1b(CC,C_dim,C)
       
       !initialize D
        call array_to_AT_tensor_1b(DD,D_dim,D)

       call at_contract(R,(/ n1,n2 /),A,(/ n3,n4,n5,n6 /),B,(/ n7,n8 /),C,(/ n9,n10 /) ,D,(/ n11,n12 /))

       m=0
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j)=RR(i,j) + fact*R%elms(m)
       ENDDO
       ENDDO
       
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(D)
       call dealloc_at_tensor(R)
       
       END subroutine contraction1b_4_2_2_2


       subroutine contraction2b_4_2(AA,A_dim,adim,BB,B_dim,bdim,RR,r1,r2,r3,r4,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10)

       implicit none

       integer,intent(in) ::adim,bdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10
       Integer,intent(in) :: r1,r2,r3,r4
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       call at_contract(R,(/ n1,n2,n3,n4 /),A,(/ n5,n6,n7,n8 /),B,(/ n9,n10 /))

       m=0
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l)=RR(i,j,k,l) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(R)
       
       END subroutine contraction2b_4_2
        
       subroutine contraction2b_2_4(AA,A_dim,adim,BB,B_dim,bdim,RR,r1,r2,r3,r4,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10)

       implicit none

       integer,intent(in) ::adim,bdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10
       Integer,intent(in) :: r1,r2,r3,r4
       real*8, intent(in), target::AA(A_dim(1),A_dim(2))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2),B_dim(3),B_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2),B_dim(3),B_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4 /) )
       
       
       !initialize A
        call array_to_AT_tensor_1b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_2b(BB,B_dim,B)
       
       call at_contract(R,(/ n1,n2,n3,n4 /),A,(/ n5,n6 /),B,(/ n7,n8,n9,n10 /))

       m=0
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l)=RR(i,j,k,l) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(R)
       
       END subroutine contraction2b_2_4
        
       subroutine contraction2b_2_2_4(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,RR,r1,r2,r3,r4,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12)

       implicit none

       integer,intent(in) ::adim,bdim,cdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12
       Integer,intent(in) :: r1,r2,r3,r4
       real*8, intent(in), target::AA(A_dim(1),A_dim(2))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2),C_dim(3),C_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2),C_dim(3),C_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4 /) )
       
       
       !initialize A
        call array_to_AT_tensor_1b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_2b(CC,C_dim,C)

       call at_contract(R,(/ n1,n2,n3,n4 /),A,(/ n5,n6 /),B,(/ n7,n8 /),C,(/ n9,n10,n11,n12 /) )

       m=0
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l)=RR(i,j,k,l) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
      
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(R)
       
       END subroutine contraction2b_2_2_4
       
       subroutine contraction2b_4_4(AA,A_dim,adim,BB,B_dim,bdim,RR,r1,r2,r3,r4,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12)

       implicit none

       integer,intent(in) ::adim,bdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12
       Integer,intent(in) :: r1,r2,r3,r4
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2),B_dim(3),B_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2),B_dim(3),B_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_2b(BB,B_dim,B)
       
       call at_contract(R,(/ n1,n2,n3,n4 /),A,(/ n5,n6,n7,n8 /),B,(/ n9,n10,n11,n12 /))

       m=0
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l)=RR(i,j,k,l) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(R)
       
       END subroutine contraction2b_4_4
        
       subroutine contraction2b_4_2_2(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,RR,r1,r2,r3,r4,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12)

       implicit none

       integer,intent(in) ::adim,bdim,cdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12
       Integer,intent(in) :: r1,r2,r3,r4
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_1b(CC,C_dim,C)

       call at_contract(R,(/ n1,n2,n3,n4 /),A,(/ n5,n6,n7,n8 /),B,(/ n9,n10 /),C,(/ n11,n12 /) )

       m=0
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l)=RR(i,j,k,l) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
      
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(R)
       
       END subroutine contraction2b_4_2_2
        
       subroutine contraction2b_4_2_4(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,RR,r1,r2,r3,r4,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14)

       implicit none

       integer,intent(in) ::adim,bdim,cdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14
       Integer,intent(in) :: r1,r2,r3,r4
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2),C_dim(3),C_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2),C_dim(3),C_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_2b(CC,C_dim,C)

       call at_contract(R,(/ n1,n2,n3,n4 /),A,(/ n5,n6,n7,n8 /),B,(/ n9,n10 /),C,(/ n11,n12,n13,n14 /) )

       m=0
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l)=RR(i,j,k,l) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
      
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(R)
       
       END subroutine contraction2b_4_2_4

       subroutine contraction2b_4_4_4(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,RR,r1,r2,r3,r4,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16)

       implicit none

       integer,intent(in) ::adim,bdim,cdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16
       Integer,intent(in) :: r1,r2,r3,r4
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2),B_dim(3),B_dim(4))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2),C_dim(3),C_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2),B_dim(3),B_dim(4) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2),C_dim(3),C_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_2b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_2b(CC,C_dim,C)

       call at_contract(R,(/ n1,n2,n3,n4 /),A,(/ n5,n6,n7,n8 /),B,(/ n9,n10,n11,n12 /),C,(/ n13,n14,n15,n16 /) )

       m=0
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l)=RR(i,j,k,l) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
      
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(R)
       
       END subroutine contraction2b_4_4_4

       subroutine contraction2b_4_2_2_2(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,DD,D_dim,ddim1,RR,r1,r2,r3,r4,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14)

       implicit none

       integer,intent(in) ::adim,bdim,cdim,ddim1
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim),D_dim(ddim1)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14
       Integer,intent(in) :: r1,r2,r3,r4
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2))
       real*8, intent(in), target::DD(D_dim(1),D_dim(2))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(D)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2) /) )
        call alloc_at_tensor(D,(/ D_dim(1),D_dim(2) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_1b(CC,C_dim,C)

       !initialize D
        call array_to_AT_tensor_1b(DD,D_dim,D)

       call at_contract(R,(/ n1,n2,n3,n4 /),A,(/ n5,n6,n7,n8 /),B,(/ n9,n10 /),C,(/ n11,n12 /),D, (/ n13,n14 /) )

       m=0
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l)=RR(i,j,k,l) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
      
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(D)
       call dealloc_at_tensor(R)
       
       END subroutine contraction2b_4_2_2_2

       subroutine contraction2b_4_2_2_4(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,DD,D_dim,ddim1,RR,r1,r2,r3,r4,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16)

       implicit none

       integer,intent(in) ::adim,bdim,cdim,ddim1
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim),D_dim(ddim1)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16
       Integer,intent(in) :: r1,r2,r3,r4
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2))
       real*8, intent(in), target::DD(D_dim(1),D_dim(2),D_dim(3),D_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(D)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2) /) )
        call alloc_at_tensor(D,(/ D_dim(1),D_dim(2),D_dim(3),D_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_1b(CC,C_dim,C)

       !initialize D
        call array_to_AT_tensor_2b(DD,D_dim,D)

       call at_contract(R,(/ n1,n2,n3,n4 /),A,(/ n5,n6,n7,n8 /),B,(/ n9,n10 /),C,(/ n11,n12 /),D, (/ n13,n14,n15,n16 /) )

       m=0
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l)=RR(i,j,k,l) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
      
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(D)
       call dealloc_at_tensor(R)
       
       END subroutine contraction2b_4_2_2_4

       subroutine contraction2b_4_2_2_2_2(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,DD,D_dim,ddim1,EE,E_dim,edim, &
        &                     RR,r1,r2,r3,r4,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16)

       implicit none

       integer,intent(in) ::adim,bdim,cdim,ddim1,edim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim),D_dim(ddim1),E_dim(edim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16
       Integer,intent(in) :: r1,r2,r3,r4
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2))
       real*8, intent(in), target::DD(D_dim(1),D_dim(2))
       real*8, intent(in), target::EE(E_dim(1),E_dim(2))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(D)
        call init_at_tensor(E)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2) /) )
        call alloc_at_tensor(D,(/ D_dim(1),D_dim(2) /) )
        call alloc_at_tensor(E,(/ E_dim(1),E_dim(2) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_1b(CC,C_dim,C)

       !initialize D
        call array_to_AT_tensor_1b(DD,D_dim,D)

       !initialize E
        call array_to_AT_tensor_1b(EE,E_dim,E)

       call at_contract(R,(/ n1,n2,n3,n4 /),A,(/ n5,n6,n7,n8 /),B,(/ n9,n10 /),C,(/ n11,n12 /),D, (/ n13,n14 /),E, (/n15,n16 /) )

       m=0
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l)=RR(i,j,k,l) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
      
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(D)
       call dealloc_at_tensor(E)
       call dealloc_at_tensor(R)
       
       END subroutine contraction2b_4_2_2_2_2

       subroutine contraction3b_4_4(AA,A_dim,adim,BB,B_dim,bdim,RR,r1,r2,r3,r4,r5,r6,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14)

       implicit none

       integer,intent(in) ::adim,bdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14
       Integer,intent(in) :: r1,r2,r3,r4,r5,r6
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2),B_dim(3),B_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4,r5,r6)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_2!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2),B_dim(3),B_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4,r5,r6 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_2b(BB,B_dim,B)
       
       call at_contract(R,(/ n1,n2,n3,n4,n5,n6 /),A,(/ n7,n8,n9,n10 /),B,(/ n11,n12,n13,n14 /))

       m=0
       DO q=1,r6
       DO p=1,r5
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l,p,q)=RR(i,j,k,l,p,q) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(R)
       
       END subroutine contraction3b_4_4

       subroutine contraction3b_4_2_4(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,RR,r1,r2,r3,r4,r5,r6,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16)

       implicit none

       integer,intent(in) ::adim,bdim,cdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16
       Integer,intent(in) :: r1,r2,r3,r4,r5,r6
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2),C_dim(3),C_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4,r5,r6)
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2),C_dim(3),C_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4,r5,r6 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_2b(CC,C_dim,C)

      call at_contract(R,(/ n1,n2,n3,n4,n5,n6 /),A,(/ n7,n8,n9,n10 /),B,(/ n11,n12 /),C,(/ n13,n14,n15,n16 /) )

       m=0
       DO q=1,r6
       DO p=1,r5
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l,p,q)=RR(i,j,k,l,p,q) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       ENDDO
      
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(R)
       
       END subroutine contraction3b_4_2_4

       subroutine contraction3b_2_4_4(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,RR,r1,r2,r3,r4,r5,r6,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16)

       implicit none

       integer,intent(in) ::adim,bdim,cdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16
       Integer,intent(in) :: r1,r2,r3,r4,r5,r6
       real*8, intent(in), target::AA(A_dim(1),A_dim(2))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2),B_dim(3),B_dim(4))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2),C_dim(3),C_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4,r5,r6)
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2),B_dim(3),B_dim(4) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2),C_dim(3),C_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4,r5,r6 /) )
       
       
       !initialize A
        call array_to_AT_tensor_1b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_2b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_2b(CC,C_dim,C)

      call at_contract(R,(/ n1,n2,n3,n4,n5,n6 /),A,(/ n7,n8 /),B,(/ n9,n10,n11,n12 /),C,(/ n13,n14,n15,n16 /) )

       m=0
       DO q=1,r6
       DO p=1,r5
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l,p,q)=RR(i,j,k,l,p,q) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       ENDDO
      
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(R)
       
       END subroutine contraction3b_2_4_4

       subroutine contraction3b_4_4_4(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,RR,r1,r2,r3,r4,r5,r6,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17,n18)

       implicit none

       integer,intent(in) ::adim,bdim,cdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17,n18
       Integer,intent(in) :: r1,r2,r3,r4,r5,r6
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2),B_dim(3),B_dim(4))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2),C_dim(3),C_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4,r5,r6)
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2),B_dim(3),B_dim(4) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2),C_dim(3),C_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4,r5,r6 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_2b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_2b(CC,C_dim,C)

      call at_contract(R,(/ n1,n2,n3,n4,n5,n6 /),A,(/ n7,n8,n9,n10 /),B,(/ n11,n12,n13,n14 /),C,(/ n15,n16,n17,n18 /) )

       m=0
       DO q=1,r6
       DO p=1,r5
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l,p,q)=RR(i,j,k,l,p,q) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       ENDDO
      
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(R)
       
       END subroutine contraction3b_4_4_4

       subroutine contraction3b_4_2_4_4(AA,A_dim,adim,BB,B_dim,bdim,CC,C_dim,cdim,DD,D_dim,ddim,RR,r1,r2,r3,r4,r5,r6,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17,n18,n19,n20)

       implicit none

       integer,intent(in) ::adim,bdim,cdim,ddim
       integer,intent(in) ::A_dim(adim),B_dim(bdim),C_dim(cdim),D_dim(ddim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14,n15,n16,n17,n18,n19,n20
       Integer,intent(in) :: r1,r2,r3,r4,r5,r6
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2))
       real*8, intent(in), target::CC(C_dim(1),C_dim(2),C_dim(3),C_dim(4))
       real*8, intent(in), target::DD(D_dim(1),D_dim(2),D_dim(3),D_dim(4))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4,r5,r6)
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(C)
        call init_at_tensor(D)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2) /) )
        call alloc_at_tensor(C,(/ C_dim(1),C_dim(2),C_dim(3),C_dim(4) /) )
        call alloc_at_tensor(D,(/ D_dim(1),D_dim(2),D_dim(3),D_dim(4) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4,r5,r6 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_1b(BB,B_dim,B)
       
       !initialize C
        call array_to_AT_tensor_2b(CC,C_dim,C)

       !initialize D
        call array_to_AT_tensor_2b(DD,D_dim,D)

     call at_contract(R,(/n1,n2,n3,n4,n5,n6/),A,(/n7,n8,n9,n10/),B,(/n11,n12/),C,(/n13,n14,n15,n16/),D,(/n17,n18,n19,n20 /))

       m=0
       DO q=1,r6
       DO p=1,r5
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l,p,q)=RR(i,j,k,l,p,q) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       ENDDO
       ENDDO
      
       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(C)
       call dealloc_at_tensor(D)
       call dealloc_at_tensor(R)
       
       END subroutine contraction3b_4_2_4_4

       subroutine contraction1b_4_6(AA,A_dim,adim,BB,B_dim,bdim,RR,r1,r2,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12)

       implicit none

       integer,intent(in) ::adim,bdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12
       Integer,intent(in) :: r1,r2
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2),B_dim(3),B_dim(4),B_dim(5),B_dim(6))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_4!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_4!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2),B_dim(3),B_dim(4),B_dim(5),B_dim(6) /) )
        call alloc_at_tensor(R,(/ r1,r2 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_3b(BB,B_dim,B)
       
       call at_contract(R,(/ n1,n2 /),A,(/ n3,n4,n5,n6 /),B,(/ n7,n8,n9,n10,n11,n12 /))

       m=0
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j)=RR(i,j) + fact*R%elms(m)
       ENDDO
       ENDDO

       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(R)
       
       END subroutine contraction1b_4_6
        
       subroutine contraction2b_2_6(AA,A_dim,adim,BB,B_dim,bdim,RR,r1,r2,r3,r4,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12)

       implicit none

       integer,intent(in) ::adim,bdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12
       Integer,intent(in) :: r1,r2,r3,r4
       real*8, intent(in), target::AA(A_dim(1),A_dim(2))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2),B_dim(3),B_dim(4),B_dim(5),B_dim(6))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_4!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_4!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2),B_dim(3),B_dim(4),B_dim(5),B_dim(6) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4 /) )
       
       
       !initialize A
        call array_to_AT_tensor_1b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_3b(BB,B_dim,B)
       
       call at_contract(R,(/ n1,n2,n3,n4 /),A,(/ n5,n6 /),B,(/ n7,n8,n9,n10,n11,n12 /))

       m=0
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l)=RR(i,j,k,l) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO

       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(R)
       
       END subroutine contraction2b_2_6
        
       subroutine contraction2b_4_6(AA,A_dim,adim,BB,B_dim,bdim,RR,r1,r2,r3,r4,fact,  &          
        &                     n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14)

       implicit none

       integer,intent(in) ::adim,bdim
       integer,intent(in) ::A_dim(adim),B_dim(bdim)
       Integer,intent(in) :: n1,n2,n3,n4,n5,n6,n7,n8,n9,n10,n11,n12,n13,n14
       Integer,intent(in) :: r1,r2,r3,r4
       real*8, intent(in), target::AA(A_dim(1),A_dim(2),A_dim(3),A_dim(4))
       real*8, intent(in), target::BB(B_dim(1),B_dim(2),B_dim(3),B_dim(4),B_dim(5),B_dim(6))
       real*8, intent(in) ::fact
       real*8, intent(inout)::RR(r1,r2,r3,r4)
       
       
!      IF (A_dim(1)*A_dim(2).NE.size(AA,1)) THEN
!      write(*,*)'BAD contraction1b_4_4!'
!      ENDIF
!      IF (A_dim(3)*A_dim(4).NE.size(AA,2)) THEN
!      write(*,*)'BAD contraction1b_4_4!'
!      ENDIF
       
        call init_at_tensor(A)
        call init_at_tensor(B)
        call init_at_tensor(R)

        call alloc_at_tensor(A,(/ A_dim(1),A_dim(2),A_dim(3),A_dim(4) /) )
        call alloc_at_tensor(B,(/ B_dim(1),B_dim(2),B_dim(3),B_dim(4),B_dim(5),B_dim(6) /) )
        call alloc_at_tensor(R,(/ r1,r2,r3,r4 /) )
       
       
       !initialize A
        call array_to_AT_tensor_2b(AA,A_dim,A)
       
       !initialize B
        call array_to_AT_tensor_3b(BB,B_dim,B)
       
       call at_contract(R,(/ n1,n2,n3,n4 /),A,(/ n5,n6,n7,n8 /),B,(/ n9,n10,n11,n12,n13,n14 /))

       m=0
       DO l=1,r4
       DO k=1,r3
       DO j=1,r2
       DO i=1,r1
       m=m+1
       RR(i,j,k,l)=RR(i,j,k,l) + fact*R%elms(m)
       ENDDO
       ENDDO
       ENDDO
       ENDDO

       call dealloc_at_tensor(A)
       call dealloc_at_tensor(B)
       call dealloc_at_tensor(R)
       
       END subroutine contraction2b_4_6
        

       subroutine array_to_AT_tensor_1b(ZZ,Zdim,Z,mae)

       implicit none

       integer, intent(in) ::Zdim(2)
       real*8,  intent(in) ::ZZ(Zdim(1),Zdim(2))
       real*8, optional    :: mae !Maximal absolute element
       TYPE(AT_tensor), intent(out) :: Z

        IF (present(mae)) THEN
          Z%mae = mae
          Z%mae_set = .TRUE.
        ENDIF

        m=0
        DO j=1,Zdim(2)
        DO i=1,Zdim(1)
        m=m+1
        Z%elms(m) = ZZ(i,j)
        ENDDO
        ENDDO


       end subroutine array_to_AT_tensor_1b

       subroutine array_to_AT_tensor_2b(ZZ,Zdim,Z,mae)

       use AT_permute_mod, only : AT_permute4_12,AT_permute4_34
       use AT_type_def,    only : AT_set_mae,AT_copy
       implicit none

       integer, intent(in) ::Zdim(4)
       real*8,  intent(in) ::ZZ(Zdim(1),Zdim(2),Zdim(3),Zdim(4))
       real*8, optional    :: mae !Maximal absolute element
       TYPE(AT_tensor), intent(out) :: Z
       !
       Integer :: IZ(4) !Dummy variable
       Integer :: z1,z2,z3,z4

       z1 = Zdim(1)
       z2 = Zdim(2)
       z3 = Zdim(3)
       z4 = Zdim(4)

       IF (present(mae)) THEN
         CALL AT_set_mae(Z,mae)
       ENDIF

       CALL AT_copy(ZZ,Z%elms,z1*z2*z3*z4)

!      !Simen: To be removed once the proper fortran order has been implemented
!      CALL AT_permute4_12(Z%elms,z2,z1,z4,z3,IZ)
!      CALL AT_permute4_34(Z%elms,z1,z2,z4,z3,IZ)

       end subroutine array_to_AT_tensor_2b

       subroutine array_to_AT_tensor_3b(ZZ,Zdim,Z,mae)

       use AT_permute_mod, only : AT_permute4_12,AT_permute4_34
       use AT_type_def,    only : AT_set_mae,AT_copy
       implicit none

       integer, intent(in) ::Zdim(6)
       real*8,  intent(in) ::ZZ(Zdim(1),Zdim(2),Zdim(3),Zdim(4),Zdim(5),Zdim(6))
       real*8, optional    :: mae !Maximal absolute element
       TYPE(AT_tensor), intent(out) :: Z
       !
       Integer :: IZ(4) !Dummy variable
       Integer :: z1,z2,z3,z4,z5,z6

       z1 = Zdim(1)
       z2 = Zdim(2)
       z3 = Zdim(3)
       z4 = Zdim(4)
       z5 = Zdim(5)
       z6 = Zdim(6)

       IF (present(mae)) THEN
         CALL AT_set_mae(Z,mae)
       ENDIF

       CALL AT_copy(ZZ,Z%elms,z1*z2*z3*z4*z5*z6)

!      !Simen: To be removed once the proper fortran order has been implemented
!      CALL AT_permute4_12(Z%elms,z2,z1,z4,z3,IZ)
!      CALL AT_permute4_34(Z%elms,z1,z2,z4,z3,IZ)

       end subroutine array_to_AT_tensor_3b


       end module contraction
